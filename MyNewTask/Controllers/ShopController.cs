﻿using MyNewTask.Models;
using MyNewTask.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyNewTask.Controllers
{
    public class ShopController : Controller
    {
        // GET: Shop
        public ActionResult Index()
        {
            MarketDBcontext db = new MarketDBcontext();
            var model = db.Product.ToList();

            return View(model);
        }       

        [HttpGet]
        public ActionResult ViewCategory()
        {
            MarketDBcontext db = new MarketDBcontext();
            var model = db.Category.ToList();

            return View(model);
        }

        [HttpGet]
        public ActionResult CreateCategory(int? id)
        {

            MarketDBcontext db = new MarketDBcontext();
            if (id.HasValue)
            {
                var model = db.Category.Find(id);               
                return View(model);
            }
           
            return View(); 
        }

        [HttpGet]
        public ActionResult DeleteCategory( int? id)
        {
            MarketDBcontext db = new MarketDBcontext();
            var model= db.Category.Find(id);

            return View(model);
        } 

        [HttpPost]
        public ActionResult DeleteCategory(int id)
        { 
            if (ModelState.IsValid)

            {
                MarketDBcontext db = new MarketDBcontext();
                var model = db.Category.Find(id);
                db.Category.Remove(model);             
                db.SaveChanges();
            }
                return RedirectToAction("ViewCategory");  
                           
        }

        [HttpPost]
        public ActionResult CreateCategory(int? id, Category model)
        {
            MarketDBcontext db = new MarketDBcontext();

            if (id.HasValue)
            {
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ViewCategory");
            }

            if (ModelState.IsValid)
            {
                db.Category.Add(model);
                db.SaveChanges();
                return RedirectToAction("ViewCategory");
            }

            return View(model);
        }


        [HttpGet]
        public ActionResult EditProduct(int? id)
        {
            MarketDBcontext db = new MarketDBcontext();

            Product productModel;
            if (id.HasValue)
            {
             productModel = db.Product.Find(id);
            //productModel = db.Product.Where(p => p.ID == id.Value).SingleOrDefault();
             ViewBag.CategorySelectList = new SelectList(db.Category.ToList(), "ID", "Name", productModel.CategoryId);
                return View(productModel);
            }            
            
            Product product = new Product();          
            
            ViewBag.CategorySelectList = new SelectList(db.Category.ToList(), "ID", "Name", product.CategoryId);
           
            return View(product);
        }

        [HttpPost]
        public ActionResult EditProduct(int? id, Product model)
        {
            MarketDBcontext db = new MarketDBcontext();
            if (id.HasValue)
            { 
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");

            }

            if (ModelState.IsValid)
            {
                db.Product.Add(model);
                db.SaveChanges();
                return RedirectToAction("index");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult DeleteProduct(int? id)
        {
            MarketDBcontext db = new MarketDBcontext();
         var model=db.Product.Find(id);

            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteProduct(int id)
        {
            MarketDBcontext db = new MarketDBcontext();
            Product product = db.Product.Find(id);
            db.Product.Remove(product);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

    }
}