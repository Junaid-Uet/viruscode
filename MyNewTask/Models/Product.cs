﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNewTask.Models
{
  public class Product
    {
        [Key]
        public int ID { get; set; }

        [Display(Name="Product Name")]
        public string Name { get; set; }

        [Display(Name = "Price")]
        public int? Price { get; set; }

        [Display(Name = "Quantity")]
        public int? Quantity { get; set; }

        [ForeignKey("Category")]
        [Required(ErrorMessage ="Category must be required field")]

        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }
    }
   
    
}
