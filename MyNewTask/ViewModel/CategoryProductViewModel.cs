﻿using MyNewTask.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNewTask.ViewModel
{
   public class CategoryProductViewModel
    {
        public int categoryID { get; set; }
        public string categoryName { get; set; }

        public int productID { get; set; }
        
        public string productName { get; set; }
        
        public int productPrice { get; set; }
       
        public int productQuantity { get; set; }        

    }
}
